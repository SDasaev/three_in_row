let playBtn = document.querySelector('.playButton')
let volumeInput = document.querySelector('.volumeInput')
let muteBtn = document.querySelector('.muteButton')
let backgroundSound = document.querySelector('#backgroundSound')
let santaSound = document.querySelector('#santaSound')
let startGameBtn = document.querySelector('.startGame')
let previousSoundBtn = document.querySelector('.previousSoundButton')
let nextSoundBtn = document.querySelector('.nextSoundButton')
let playClickCounter = 0
let muteClickCounter = 0
let audioCounter = 1;
backgroundSound.volume = 0.2

startGameBtn.addEventListener('click', () => {
  playClickCounter++
  backgroundSound.play()
  playBtn.classList.remove('play')
  playBtn.classList.add('stop')
})

playBtn.addEventListener('click', () => {
  if (playClickCounter % 2 === 0 ) {
    playBtn.classList.remove('play')
    playBtn.classList.add('stop')
    backgroundSound.play()
  } else {
    playBtn.classList.remove('stop')
    playBtn.classList.add('play')
    backgroundSound.pause()
  }
  playClickCounter++
})

volumeInput.addEventListener('input', () =>{
  backgroundSound.volume = volumeInput.value
})

muteBtn.addEventListener('click', ()=> {
  if (muteClickCounter % 2 === 0 ) {
    muteBtn.classList.remove('notMute')
    muteBtn.classList.add('mute')
    backgroundSound.muted = true
    santaSound.muted = true
  } else {
    muteBtn.classList.remove('mute')
    muteBtn.classList.add('notMute')
    backgroundSound.muted = false
    santaSound.muted = false
  }
  muteClickCounter++
})

previousSoundBtn.addEventListener('click', () => {
  audioCounter--
  if (audioCounter === 0) {
    audioCounter = 3
  }
  backgroundSound.src = `./sounds/${audioCounter}.mp3`
  backgroundSound.play()
})

nextSoundBtn.addEventListener('click', () => {
  audioCounter++
  if (audioCounter === 4) {
    audioCounter = 1
  }
  backgroundSound.src = `./sounds/${audioCounter}.mp3`
  backgroundSound.play()
})
