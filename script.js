let scoreElem = document.querySelector('.score')
let gameTable = document.querySelector('.gameTable')
let sizeInput = document.querySelector('.sizeInput')
let startBtn = document.querySelector('.startGame')
let items = ['tree', 'toy', 'gift', 'snow']
let pointsElem = document.querySelector('.points')



let game = {
  row: [],
  col: [],
  isMarkedGameField: [],
  isAnimDone: [],
  isPlay: false,
  score: 0,
  size: 0,
  createTable: function () {
    this.isPlay = false
    this.score = 0
    this.previousScore = 0
    this.scoreForSound = 0
    this.bestCombination = 0
    this.resetTable()
    scoreElem.textContent = `Счет: ${this.score}`
    this.size = +sizeInput.value * 2

    for (let i = 0; i < this.size; i++) {
      let row = document.createElement('tr')
      this.isMarkedGameField.push(new Array)
      for (let j = 0; j < this.size / 2; j++) {
        let cell = document.createElement('td')
        cell.dataset.index = i * this.size + j
        cell.classList.add('cell')
        cell.style.top = `${i * 60}px`
        cell.style.left = `${j * 60}px`
        let cellDiv = document.createElement('div')
        cellDiv.classList.add('cellDiv')
        cellDiv.dataset.index = i * this.size + j
        cellDiv.style.backgroundImage = `url(images/items/unknown.png)`
        if (i > this.size / 2 - 1) {
          cell.addEventListener('click', listener)
          cellDiv.addEventListener('click', listener)
        }
        cell.appendChild(cellDiv)
        row.appendChild(cell)
        this.setMarkOnGameField(i, j)
      }
      gameTable.appendChild(row)
    }
    gameTable.style.top = -gameTable.clientHeight / 2 + 'px'

    this.showCellsAnim()

    function listener() {
      pointsElem.style.left = gameTable.getBoundingClientRect().left + gameTable.getBoundingClientRect().width + 15 + 'px'
      if (game.isAnimDone.length === 0) {
        let i = Math.floor(this.dataset.index / game.size)
        let j = this.dataset.index % game.size
        if (game.isPlay === true) {
          game.clickOnCell(i, j)
        }
      }
    }
  },
  resetTable: function () {
    let i = gameTable.rows.length
    while (i !== 0) {
      gameTable.rows[i - 1].remove()
      i--
      this.isMarkedGameField.pop()
    }
  },
  setMarkOnGameField: function (i, j) {
    if (i >= game.size / 2) {
      game.isMarkedGameField[i][j] = false
    } else {
      game.isMarkedGameField[i][j] = ''
    }
  },
  getItemImage: function () {
    return items[Math.floor(Math.random() * 4)]
  },
  showCellsAnim: function () {
    let cells = gameTable.getElementsByClassName('cellDiv')
    for (let k = 0; k < cells.length; k++) {
      cells[k].style.opacity = '0'
      if (game.isPlay === true) {
        cells[k].style.backgroundImage = `url(images/items/${this.getItemImage()}.png)`
      }
    }
    showCellsAnimId = setInterval(() => {
      for (let i = 0; i < cells.length; i++) {
        if (cells[i].style.opacity !== '1') {
          cells[i].style.opacity = +cells[i].style.opacity + 0.01
        } else {
          clearInterval(showCellsAnimId)
        }
      }
    }, 20)
  },
  moveAnim: function (k, j) {
    game.isAnimDone.push(false)
    let counter = 0
    let moveAnimId = setInterval(() => {
      if (counter < 54) {
        gameTable.rows[k - 1].cells[j].children[0].style.top = +gameTable.rows[k - 1].cells[j].children[0].style.top.slice(0, -2) + 9 + 'px'
        counter += 9
      } else {
        gameTable.rows[k].cells[j].children[0].style.backgroundImage = gameTable.rows[k - 1].cells[j].children[0].style.backgroundImage
        gameTable.rows[k - 1].cells[j].children[0].style.top = null
        gameTable.rows[k].cells[j].children[0].style.opacity = '1'
        if (k === 1) {
          gameTable.rows[0].cells[j].children[0].style.backgroundImage = `url(images/items/${this.getItemImage()}.png)`
        }
        game.isAnimDone.pop()
        clearInterval(moveAnimId)
      }
    }, 23)
  },
  clickOnCell: function (row, col) {
    this.row.push(row)
    this.col.push(col)
    this.isMarkedGameField[row][col] = true

    for (let i = 0; i < this.row.length; i++) {
      compareSurroundCells(this.row[i], this.col[i])
    }

    function compareSurroundCells(row, col) {
      compareTopCell.call(game, row, col)
      compareRightCell.call(game, row, col)
      compareBottomCell.call(game, row, col)
      compareLeftCell.call(game, row, col)

      function compareTopCell(row, col) {
        if (row === game.size / 2) return
        if (gameTable.rows[row].cells[col].children[0].style.backgroundImage === gameTable.rows[row - 1].cells[col].children[0].style.backgroundImage && this.isMarkedGameField[row - 1][col] === false) {
          pushIntoGame(row - 1, col)
        }
      }

      function compareRightCell(row, col) {
        if (col === game.size / 2 - 1) return
        if (gameTable.rows[row].cells[col].children[0].style.backgroundImage === gameTable.rows[row].cells[col + 1].children[0].style.backgroundImage && this.isMarkedGameField[row][col + 1] === false) {
          pushIntoGame(row, col + 1)
        }
      }

      function compareBottomCell(row, col) {
        if (row === game.size - 1) return
        if (gameTable.rows[row].cells[col].children[0].style.backgroundImage === gameTable.rows[row + 1].cells[col].children[0].style.backgroundImage && this.isMarkedGameField[row + 1][col] === false) {
          pushIntoGame(row + 1, col)
        }
      }

      function compareLeftCell(row, col) {
        if (col === 0) return
        if (gameTable.rows[row].cells[col].children[0].style.backgroundImage === gameTable.rows[row].cells[col - 1].children[0].style.backgroundImage && this.isMarkedGameField[row][col - 1] === false) {
          pushIntoGame(row, col - 1)
        }
      }

      function pushIntoGame(row, col) {
        game.row.push(row)
        game.col.push(col)
        game.isMarkedGameField[row][col] = true
      }
    }

    if (this.row.length > 2) {
      pointsElem.textContent = `+${this.row.length * 10}`
      this.score += this.row.length * 10
      if (this.bestCombination < this.row.length * 10) {
        this.bestCombination = this.row.length * 10
      }
      scoreElem.textContent = `Счет: ${this.score}. Лучшая комбинация: ${this.bestCombination}`

      if (this.score - this.scoreForSound >= 1000) {
        let santaSound = document.getElementById('santaSound')
        santaSound.volume = 0.4
        santaSound.play()
        this.scoreForSound = Math.floor(this.score / 1000) * 1000
      }

      pointsAnim()

      function pointsAnim() {
        game.isAnimDone.push(false)
        let isFirstDestination = false
        let pointsAnimId = setInterval(() => {

          if (isFirstDestination === false) {
            if (pointsElem.getBoundingClientRect().top < 40) {
              pointsElem.style.top = pointsElem.getBoundingClientRect().top + 7 + 'px'
            } else isFirstDestination = true
          } else if (pointsElem.getBoundingClientRect().top > -100) {
            pointsElem.style.top = pointsElem.getBoundingClientRect().top - 7 + 'px'
          } else {
            game.isAnimDone.pop()
            clearInterval(pointsAnimId)
          }
        }, 25)
      }

      destroyEqual.call(game)

      function destroyEqual() {
        for (let i = this.size / 2; i < this.size; i++) {
          for (let j = 0; j < this.size / 2; j++) {
            if (this.isMarkedGameField[i][j] === true) {
              gameTable.rows[i].cells[j].children[0].style.opacity = 0
              for (let k = i; k > 0; k--) {
                this.moveAnim(k, j)
              }
            }
          }
        }
      }
    }
    this.row = []
    this.col = []
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size / 2; j++) {
        this.setMarkOnGameField(i, j)
      }
    }
  }
}

sizeInput.addEventListener('change', game.createTable.bind(game))
startBtn.addEventListener('click', () => {
  game.isPlay = true
  game.showCellsAnim()
})

game.createTable()